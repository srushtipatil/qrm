
# coding: utf-8

# ## Objective: Figure out format in df_QRR['Risk Identification Source']

# In[1]:


from importlib import reload
import os
import re 
import numpy as np
import pandas as pd
from datetime import datetime
from openpyxl import Workbook
from openpyxl import load_workbook
import FMEA
import PHA
import WHATIF
import SMC_QRA


reload (FMEA)
reload (PHA)
reload (WHATIF)
reload (SMC_QRA)

pd.set_option("display.max_columns", None) 


# In[2]:


## Determine assessment type for unknown files
all_QRA_types = {'PHA':'PHA', 'FMEA':'FMEA', 'Gap & Risk Assessment': 'SMC_QRA', 'What-If': 'WHAT-IF'}
os.chdir("C:\\Users\\sinhanis\\Documents\\QRM\\Python Scripts")

def getQRAtype(QRAfile):    
    tab_names = pd.read_excel(QRAfile, None).keys()
    QRAtype = ''
    for x in all_QRA_types.keys():
        if x in tab_names:
            QRAtype = x
            break
    if QRAtype:
        return QRAtype
    else:
        return str([tab for tab in tab_names])


# ### Get all files from folder

# In[3]:


# path = os.getcwd() + os.path.sep + 'Details' + os.path.sep + 'Test files 2'
# path = '.' + os.path.sep + 'Details' + os.path.sep + 'Test files 2'

## path has to be short because some file names are very long and will exceed Window path limit (260 characters) 
path = 'QRA\\'
print(path)

all_filenames = os.listdir(path)

## find old Excel files
files_to_process = []
old_xlsb = []
for file in all_filenames:
    # if len(file) <180:
    if 'xlsb' not in file:
        files_to_process += [file]
    else:
         old_xlsb += [file]

print (len(files_to_process))
print (len(old_xlsb))

### Get QRA types for all files
# In[4]:


import xlrd
xlrd.__VERSION__


# In[ ]:


dict_filename_type = {}
corrupted_files = {}
for file in files_to_process:
    try:
        dict_filename_type[file] = getQRAtype(path + os.path.sep + file)
    except Exception as e:
        corrupted_files[file] = e
# dict_filename_type = {file : getQRAtype(path + os.path.sep + file) for file in files_to_process}        
print(len(files_to_process))


# In[ ]:


len(dict_filename_type)


# In[7]:


total = len(dict_filename_type)
df = pd.DataFrame(columns = ['filename','QRA type'])
df['filename'] = dict_filename_type.keys()

df['QRA type'] = dict_filename_type.values()
df['QRA type'].nunique()
df.head()


# ### Try subset of files

# In[8]:


## Process all the unsuccessful files
excel_output = 'C:\\Users\\sinhanis\\Documents\\QRM\\Python Scripts\\Test_Summary_05212019.xlsx'
df_no_output = pd.read_excel(excel_output, sheet_name = 'Unsuccessful Output', header = 0)
df_no_output['int issue'] = df_no_output['process'].apply(lambda a: ' invalid literal for int() with base 10' in a)
file_all_int_issue = df_no_output[df_no_output['int issue'] == True]['filename'].tolist()
len(file_all_int_issue)


# In[ ]:


## Process all files; Seperate files by processing result
nonStandard_files = {}
processed_files = {}

# for file in file_all_int_issue:
for file in dict_filename_type.keys():
    temp_path = 'C:\QRA' + os.path.sep + file
    if dict_filename_type[file] == 'FMEA':
        try:
            FMEA.parser(temp_path)
            processed_files[file] = 'output file: {x}'.format(x = dict_filename_type[file])
        except Exception as e:
            nonStandard_files[file] = 'Cannot process - ' + str(e)
            # nonStandard_files[file] = 'Cannot process - ' + str(dict_filename_type[file]) 
    elif dict_filename_type[file] == 'PHA':
        try:
            PHA.parser(temp_path)
            processed_files[file] = 'output file: {x}'.format(x = dict_filename_type[file])
        except Exception as e:
            nonStandard_files[file] = 'Cannot process - ' + str(e)
            # nonStandard_files[file] = 'Cannot process - ' + str(dict_filename_type[file])
    elif dict_filename_type[file] == 'What-If':
        try:
            WHATIF.parser(temp_path)
            processed_files[file] = 'output file: {x}'.format(x = dict_filename_type[file])
        except Exception as e:
            nonStandard_files[file] = 'Cannot process - ' + str(e)
            # nonStandard_files[file] = 'Cannot process - ' + str(dict_filename_type[file])
    elif dict_filename_type[file] == 'Gap & Risk Assessment':
        try:
            SMC_QRA.parser(temp_path)
            processed_files[file] = 'output file: {x}'.format(x = dict_filename_type[file])
        except Exception as e:
            nonStandard_files[file] = 'Cannot process - ' + str(e)
            # nonStandard_files[file] = 'Cannot process - ' + str(dict_filename_type[file])
    else:
        nonStandard_files[file] = dict_filename_type[file]


# In[ ]:


def blank_score(data):
    if not str(data).strip():
        return 'low'
    else:
        return data

blank_score('   ')


# In[ ]:


df_int_issue_0520 = pd.DataFrame(columns = ['filename','process result'])
df_int_issue_0520['filename'] = nonStandard_files.keys()
df_int_issue_0520['process result'] = nonStandard_files.values()
df_int_issue_0520['process result'].value_counts()


# ### Excel output of process result

# In[ ]:


# nonStandard_files.update(processed_files)
df_nonStandard = pd.DataFrame(columns = ['filename', 'process'])
df_nonStandard['filename'] = nonStandard_files.keys()
df_nonStandard['process'] = nonStandard_files.values()

df_processed = pd.DataFrame(columns = ['filename', 'process'])
df_processed['filename'] = processed_files.keys()
df_processed['process'] = processed_files.values()

df_all = pd.concat([df_processed, df_nonStandard])
df_final = df_all.merge(df, how = 'left', on = 'filename')
df_final.head()


# In[ ]:


assert (len(nonStandard_files) + len(processed_files) == df_final.shape[0])
df_final.shape


# In[ ]:


df_summary = df_final['process'].value_counts().to_frame().reset_index()
df_summary = df_summary.rename({'index': 'Result', 'process': 'Count'}, axis = 1)
df_summary


# In[ ]:


## Label files based on process result
def process_result(x):
    if 'Cannot process' in x:
        return 'Processing issue'
    else:
        if 'output file:' in x:
            return 'Successful output'
        else:
            return 'Cannot determine QRA type'

df_final['Result type'] = df_final['process'].apply(lambda x: process_result(x))
df_final['Result type'].value_counts()


# In[ ]:


df_final.head()


# In[ ]:


# Files with no QRA type
df_NoType = df_final[df_final['Result type'] == 'Cannot determine QRA type'].rename({'process': 'all tab names'}, axis = 1)
# df_NoType = df_NoType.drop(['Result type'], axis = 1).rename({'process': 'all tab names'}, axis = 1)

# Files with successful output
df_success = df_final[df_final['Result type'] == 'Successful output']
# df_success = df_success.drop(['Result type'], axis = 1)

# Files with QRA type but unsucessful processing
df_process_issue = df_final[df_final['Result type'] == 'Processing issue']
# df_process_issue = df_process_issue.drop(['Result type'], axis = 1)


# In[ ]:


excel_output = 'Test_Summary_05212019.xlsx'
writer = pd.ExcelWriter(excel_output)
df_final.to_excel(writer, sheet_name = 'All Results', index = False)
df_summary.to_excel(writer, sheet_name = 'Summary', index = False)
df_NoType.to_excel(writer, sheet_name = 'Cannot Find Type', index = False)
df_success.to_excel(writer, sheet_name = 'Successful Output', index = False)
df_process_issue.to_excel(writer, sheet_name = 'Unsuccessful Output', index = False)


# ### Look at two major processing issues: 1)datetime input, 2)cannot find site in masterlog -- assessment_id format

# In[ ]:


## files with date issues from test summary
excel_output = 'Test_Summary_05212019.xlsx'
df_no_output = pd.read_excel(excel_output, sheet_name = 'Unsuccessful Output', header = 0)
mask = df_no_output['process'] == "Cannot process - local variable 'latest' referenced before assignment" 
file_date_issue = df_no_output['filename'][mask].tolist()
len(file_date_issue)
df_no_output.head()


# In[ ]:


dict_filename_type['EKV-QRA-2017-005 FMEA Template Potential Sterility impact due to B5H Sterile Vessel Agitator Single Mechanical Seals FMEA Spreadsheet_090143508574b5cf.xlsm']


# In[ ]:


##  Get list of files with the two issues
df_dateissue = df_no_output[df_no_output['process'] == "Cannot process - local variable 'latest' referenced before assignment"]
file_date_issue = df_dateissue['filename'].tolist()

df_siteissue = df_no_output[df_no_output['process'] == "Cannot process - index 0 is out of bounds for axis 0 with size 0"]
file_site_issue = df_siteissue['filename'].tolist()

df_assessid_issue = df_no_output[df_no_output['process'] == "Cannot process - single positional indexer is out-of-bounds"]
file_assessid_issue = df_assessid_issue['filename'].tolist()

df_int_issue = df_no_output[df_no_output['process'] == "Cannot process - invalid literal for int() with base 10: ''"]
file_int_issue = df_int_issue['filename'].tolist()

df_other_int_issue = df_no_output[df_no_output['process'] == "Cannot process - invalid literal for int() with base 10: '例\n記載内容説明'"]
file_other_int_issue = df_other_int_issue['filename'].tolist()

# print (len(file_date_issue))
# print (len(file_site_issue))
# print (len(file_assessid_issue))
len(file_other_int_issue)


# ### Other convert to int issues

# ### Cannot convert to int: Line ID input empty (some files have empty rows for format reason(?))

# In[ ]:


### ATtempt to fix: drop rows where line id is empty (df_QRA_Main read-in parameters changed)
len(file_int_issue)
len(dict_filename_type)


# In[ ]:


### Testing if dropna fixes issues with some file

# Test ALL files
int_fixed = {}
int_not_fixed = {}

# for file in process_list:
for file in file_int_issue:
    temp_path = 'C:\QRA' + os.path.sep + file
    # temp_path = '.' + os.path.sep + 'Details' + os.path.sep + 'Test files_batch1' + os.path.sep + file
    if dict_filename_type[file] == 'FMEA':
        try:
            FMEA.parser(temp_path)
            int_fixed[file] = 'output file: {x}'.format(x = dict_filename_type[file])
        except Exception as e:
            int_not_fixed[file] = 'Cannot process - ' + str(e)
            # nonStandard_files[file] = 'Cannot process - ' + str(dict_filename_type[file]) 
    elif dict_filename_type[file] == 'PHA':
        try:
            PHA.parser(temp_path)
            int_fixed[file] = 'output file: {x}'.format(x = dict_filename_type[file])
        except Exception as e:
            int_not_fixed[file] = 'Cannot process - ' + str(e)
            # nonStandard_files[file] = 'Cannot process - ' + str(dict_filename_type[file])
    elif dict_filename_type[file] == 'What-If':
        try:
            WHATIF.parser(temp_path)
            int_fixed[file] = 'output file: {x}'.format(x = dict_filename_type[file])
        except Exception as e:
            int_not_fixed[file] = 'Cannot process - ' + str(e)
            # nonStandard_files[file] = 'Cannot process - ' + str(dict_filename_type[file])
    elif dict_filename_type[file] == 'Gap & Risk Assessment':
        try:
            SMC_QRA.parser(temp_path)
            int_fixed[file] = 'output file: {x}'.format(x = dict_filename_type[file])
        except Exception as e:
            int_not_fixed[file] = 'Cannot process - ' + str(e)
            # nonStandard_files[file] = 'Cannot process - ' + str(dict_filename_type[file])
    else:
        print ('New Issue - QRAType')


# In[ ]:


from collections import Counter
Counter(int_fixed.values())


# ### Assessment ID issue (cannot match in QRR)

# In[ ]:


MasterLog_path = 'Merck_QRM_project' + os.path.sep + 'QRA_MasterLog_0423_2019.xlsx'
df_MasterLog = pd.read_excel(MasterLog_path, header = 0).rename(lambda n: n.strip(), axis = 1)


def findSite(assessment_id):
    try:
        site = df_MasterLog[df_MasterLog['QRA ID'] == assessment_id]['Site'].values[0]  ## Should come from the master log (QRR only has high/med risks)
        return site
    except:
        pass


# In[ ]:


assessid_site = {}
data_scopepage_site = {}
for file in file_assessid_issue:
    # print (file)
    temp_path = 'QRA\\' + os.path.sep + file
    if dict_filename_type[file] == 'Gap & Risk Assessment':
        df_QRA_Scope = pd.read_excel(temp_path, sheet_name = "Scope", header = None, index_col = 0, usecols = [1, 2], squeeze = True, keep_default_na = False)       
        assessment_id = df_QRA_Scope[2]
        site = findSite(assessment_id)
        assessid_site[file] = site
    else:    
        df_QRA_Scope = pd.read_excel(temp_path, sheet_name = "Scope", header = None, index_col = 0, usecols = [0, 1], squeeze = True)        
        assessment_id = df_QRA_Scope[1]
        site = findSite(assessment_id)
        assessid_site[file] = site


# In[ ]:


# print (set(assessid_site.values()), len(set(assessid_site.values())))
df_assessid_site = pd.DataFrame(columns = ['filename', 'QRR site'])
df_assessid_site['filename'] = assessid_site.keys()
df_assessid_site['QRR site'] = assessid_site.values()
df_assessid_site['QRR site'].value_counts()
file_SIN = df_assessid_site[df_assessid_site['QRR site'] == "Midrand"]['filename'].tolist()
dict_why_not_in_QRR = {'Pandaan': 'abnormal format in site QRR','West Point': 'for all 4 cases: assessment id not in QRR', 'Singapore': 'one abnormal row, the rest not in QRR'}


# In[ ]:


df_assessid_site_summary = df_assessid_site['QRR site'].value_counts().to_frame().reset_index()
df_assessid_site_summary = df_assessid_site_summary.rename({'index': 'QRR site', 'QRR site': 'Count'}, axis = 1)
df_assessid_site_summary.to_excel('Site QRR assessid issue.xlsx', index = False)


# In[ ]:


for file in file_SIN:
    if dict_filename_type[file] == "PHA":
        print (file)
file_SIN


# ### Site issues (aka., assessment_id not in QRR)

# In[ ]:


## Print assessment_id input and whether found in masterLog
file_site_issue=""
file_site_issue_new=""

site_index = {'PHA': 1, 'FMEA': 1, 'Gap & Risk Assessment': 2, 'What-If': 1}
id_inLog = df_MasterLog['QRA ID'].tolist()

assessmentID_scopepage = {}
assessmentID_inLog = {}
input_type = {}
for file in file_site_issue_new:
    # print (file)
    temp_path = 'QRA' + os.path.sep + file
    if dict_filename_type[file] == 'Gap & Risk Assessment':
        df_QRA_Scope = pd.read_excel(temp_path, sheet_name = "Scope", header = None, index_col = 0, usecols = [1, 2], squeeze = True, keep_default_na = False)
        assessmentID_scopepage[file] = df_QRA_Scope[2]
        input_type[file] = type(df_QRA_Scope[2])
        if len(str(df_QRA_Scope[2])) > 3:
            assessmentID_inLog[file] = df_QRA_Scope[2] in id_inLog
        else:
            assessmentID_inLog[file] = ''
        # assessment_id = df_QRA_Scope[2]
        # site = findSite(assessment_id)
        # data_scopepage_site[file] = site
    else:    
        df_QRA_Scope = pd.read_excel(temp_path, sheet_name = "Scope", header = None, index_col = 0, usecols = [0, 1], squeeze = True)
        assessmentID_scopepage[file] = df_QRA_Scope[1]
        input_type[file] = type(df_QRA_Scope[1])
        if len(str(df_QRA_Scope[1])) > 3:
            assessmentID_inLog[file] = df_QRA_Scope[1] in id_inLog
        else:
            assessmentID_inLog[file] = ''
            
        # assessment_id = df_QRA_Scope[1]
        # site = findSite(assessment_id)
        # data_scopepage_site[file] = site


# In[ ]:


assert(len(assessmentID_scopepage) == len(file_site_issue_new))


# In[ ]:


# len(assessmentID_scopepage) == len(file_site_issue)
df_assessmentID_scopepage = pd.DataFrame(columns = ['filename', 'assessment_id'])
df_assessmentID_scopepage['filename'] = assessmentID_scopepage.keys()
df_assessmentID_scopepage['assessment_id'] = assessmentID_scopepage.values()

df_assessmentID_inLog = pd.DataFrame(columns = ['filename', 'found in log'])
df_assessmentID_inLog['filename'] = assessmentID_inLog.keys()
df_assessmentID_inLog['found in log'] = assessmentID_inLog.values()


df_site_issue = df_assessmentID_scopepage.merge(df_assessmentID_inLog, how = 'left', on = 'filename')
df_site_issue.to_excel('Site_issues.xlsx', index = False)


# In[ ]:


df_site_issue['found in log'].value_counts()


# In[ ]:


## Testing with single files
test_file = 'SIN-QRA-2017-0026_0901435085437d39.xlsm'
path = 'QRA' + os.path.sep + test_file
df_QRA_Scope = pd.read_excel(path, sheet_name = "Scope", header = None, index_col = 0, usecols = [0, 1], squeeze = True)
str(df_QRA_Scope[1]).strip() in id_inLog


# In[ ]:


df_site_not_fixed = pd.DataFrame(columns = ['filename', 'process'])


# ### -- Try to process single files (for quick testing)

# In[ ]:
"""

df_site_not_fixed['filename'] = site_not_fixed.keys()
df_site_not_fixed['process'] = site_not_fixed.values()
df_site_not_fixed['process'].value_counts()
df_xxx = df_site_not_fixed[df_site_not_fixed['process'] == "Cannot process - index 0 is out of bounds for axis 0 with size 0"]
file_site_issue_new = df_xxx['filename'].tolist()
len(file_site_issue_new)

"""
# In[ ]:


x = 'SINGAPORE_SIN-QRA-2017-0029 SMC QRA FOR INTRODUCTION OF KEYTRUDA INTO THE KEYTRUDA FILLING LINE IN THE SOUTH BLOCK BIOTECH BUILDING MSD SINGAPORE_09014350860bf075.xlsx'
len(x)


# In[ ]:


### Test one by one
# dict_filename_type_test = {'SIN-QRA-2017-0019 (FMEA).xlsm': 'FMEA'}
# dict_filename_type_test = {'BRI-QRA-2018-0012 FMEA_09014350859e0ff0.xlsm': 'FMEA'}
# dict_filename_type_test = {'SINGAPORE_SIN-QRA-2017-0029 SMC QRA FOR INTRODUCTION OF KEYTRUDA INTO THE KEYTRUDA FILLING LINE IN THE SOUTH BLOCK BIOTECH BUILDING MSD SINGAPORE_09014350860bf075.xlsx': 'Gap & Risk Assessment'}
# dict_filename_type_test = {'BALLYDINE_RA-17-003 API PDC DORAVIRINE QRA FOR HMT_0901435086051e88.xlsx': 'Gap & Risk Assessment'}


# dict_filename_type_test = {'BALLYDINE_RA-17-003 API PDC DORAVIRINE QRA FOR HMT_0901435086051e88.xlsx': 'Gap & Risk Assessment'}
dict_filename_type_test = {'BAL-QRA-2018-0026 Raltegravir  Free Phenol Quality Risk Assessment_09014350863ab7fc.xlsx': 'Gap & Risk Assessment'}
# dict_filename_type_test = {'WPP-QRA-2017-0014_090143508586ea3f.xlsx': 'Gap & Risk Assessment'}
#API CO1 RA-12-009 AMILORIDE HCL PURE CODE 89 SITE BASED RISK ASSESSMENT & CPV PLAN_0901435085f91c71
#dict_filename_type_test = {'huhh.xls': 'FMEA'}

os.chdir('C:\\Users\\sinhanis\\Documents\\QRM\\Python Scripts\\QRA\\')
# dict_filename_type_test = {'BALLYDINE_BAL-QRA-2018-0031 API ADC LAB WEIGHING QUALITY RISK ASSESSMENT_0901435085e4200c.xlsm': 'PHA'}
for file in dict_filename_type_test.keys():
    temp_path = file
    print(temp_path)
    print(os.path.isfile(temp_path))
    #     temp_path = '.' + os.path.sep + 'Details' + os.path.sep + 'Test files 2' + os.path.sep + file
    """
    if dict_filename_type_test[file] == 'FMEA':
    # if dict_filename_type_test[file] == 'Gap & Risk Assessment':
        FMEA.parser(temp_path)
    if dict_filename_type_test[file] == 'PHA':
    # if dict_filename_type_test[file] == 'Gap & Risk Assessment':
        PHA.parser(temp_path)
        # SMC_QRA.parser(temp_path)
    """
    if dict_filename_type_test[file] == 'Gap & Risk Assessment':
        print("Hi")
        if os.path.isfile(temp_path) == True :
            SMC_QRA.parser(temp_path)

