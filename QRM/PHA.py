import os
import re
import shutil  # for high-level operations on files and collections of files
import numpy as np
import pandas as pd
from datetime import datetime
from openpyxl import Workbook
from openpyxl import load_workbook
pd.set_option("display.max_columns", None) 

def parser(input_file, filename):
    ### Read in everything from template

    ## Read in the template
    temp_path = '.' + os.path.sep + 'Details' + os.path.sep + 'Test files 2'
    QRA_temp_path = '.' + os.path.sep + 'Details' + os.path.sep + 'RMS-QRM Data Migration Template 17Apr2019 field mapping.xlsx'
    QRA_temp_dict = pd.read_excel(QRA_temp_path, sheet_name=None)


    ## names of all worksheets
    all_tabs_temp = QRA_temp_dict.keys()

    ## worksheets we are mapping
    yellow_tabs = ['Plan Header', 'Assessment', 'Control Rating', 'Mitigating Actions', 'QRM Risk Register']


    ## Template tabs with two/one rows as headers
    double_tabs = ['Plan Header', 'Plan Assessments', 'Plan-QRM Team Details', 'Assessment'] 
    single_tabs = ['Control Rating', 'Mitigating Actions', 'Report Details', 'Report Reviewers - Approvers', 'Report QRA Participants ', 'QRM Risk Register']
    tabs = double_tabs + single_tabs

    tabHeader_dict = {**{tab: 1 for tab in double_tabs}, **{tab: 0 for tab in single_tabs}}

    ## every tab and its columns in template
    dict_tab_col = {tab: pd.DataFrame(columns=pd.read_excel(QRA_temp_path, sheet_name=tab, header=tabHeader_dict[tab], index_col=None).columns.str.strip()) for tab in tabs}

    # dict_tab_col.keys()


    ### Read everything in QRA and Register files

#     QRA_file = 'WPP-QRA-2018-0081 B62 Weigh and Dispense (D199) Cross Contamination PHA QRA Revision 0_09014350864b024c.xlsm'
#     QRA_file_path = temp_path + os.path.sep + QRA_file
    QRA_file_path = input_file


    ## Read in master log
    MasterLog_path = '.' + os.path.sep + 'Details' + os.path.sep + 'QRA_MasterLog_0423_2019.xlsx'
    df_MasterLog = pd.read_excel(MasterLog_path, header = 0).rename(lambda n: n.strip(), axis = 1)
    df_MasterLog['QRA ID'] = df_MasterLog['QRA ID'].apply(lambda x: str(x).strip())
    
    ## Read in site and product lists for cross-checking
    site_path = '.' + os.path.sep + 'Details' + os.path.sep + 'Site_List.xlsx'
    product_path = '.' + os.path.sep + 'Details' + os.path.sep + 'Product_List.xlsx'

    df_sitelist = pd.read_excel(site_path, header = 0)
    sitelist = {df_sitelist['Incoming'][i]: df_sitelist['Site Name'][i] for i in range(df_sitelist.shape[0])}

    df_productlist = pd.read_excel(product_path, header = 0)
    productlist = {df_productlist['Incoming'][i]: df_productlist['Product Name'][i] for i in range(df_productlist.shape[0])}


    ## Determine assessment type for unknown files
    all_QRA_types = {'PHA':'PHA', 'FMEA':'FMEA', 'Gap & Risk Assessment': 'SMC QRA', 'What-If': 'WHAT-IF'}

    def getQRAtype(QRAfile):    
        tab_names = pd.read_excel(QRAfile, None).keys()
        QRAtype = ''
        for x in all_QRA_types.keys():
            if x in tab_names:
                QRAtype = x
                break
        if QRAtype:
            return QRAtype
        else:
            return 'Cannot find type'


    QRA_type = getQRAtype(QRA_file_path)
    print (QRA_type)

    
    def check_Numeric(data):
        try:
            np.float(data)
            return data
        except:
            return np.nan

    def is_empty(data):
        if not data:
            return True
        elif not str(data).strip():
            return True
        elif str(data).strip().lower() == 'nan':
            return True
        else:
            return False
    ## Read in QRA file (no need to read approval page)
    # file_ApprovalPage_df = pd.read_excel(QRA_file_path, sheet_name = "Approval Page", header = None, index_col = None)

    df_QRA_Scope = pd.read_excel(QRA_file_path, sheet_name = "Scope", header = None, index_col = 0, usecols = [0, 1], squeeze = True)
#     df_QRA_Main = pd.read_excel(QRA_file_path, sheet_name = QRA_type, header = [0, 1], keep_default_na = False, na_values=['']).rename(lambda n: n.strip(), axis = 1).reset_index()
    df_QRA_Main = pd.read_excel(QRA_file_path, sheet_name = QRA_type, header = [0, 1], keep_default_na = False, na_values = '').rename(lambda n: n.strip(), axis = 1).reset_index().rename({'index': 'Line ID'}, axis=1).dropna(subset=[('Line ID', '')])

    ## Look for non-numeric line id
    df_QRA_Main[('Line ID', '')] = df_QRA_Main[('Line ID', '')].apply(check_Numeric)

    
    ## Leaving out rows with index as NaN
    df_QRA_Main = df_QRA_Main[df_QRA_Main[('Line ID', '')].notnull()]
    #    df_QRA_Main = df_QRA_Main[df_QRA_Main['index'].notnull()]


    # df_QRA_Main.info()

    ## Determine which QRR file to use 
    # assessment_id = str(df_QRA_Scope[1]).strip()
    # site = df_MasterLog[df_MasterLog['QRA ID'] == assessment_id]['Site'].values[0]  ## Should come from the master log (QRR only has high/med risks)

    ## Read in Assessment_id cross-check list
    site_crosscheck_path = '.' + os.path.sep + 'Details' + os.path.sep + 'Assessment_ID_CrossReference.xlsx'

    df_correctedID = pd.read_excel(site_crosscheck_path, sheet_name = 'Sheet1', header = 0)
    df_correctedID_fromFile = pd.read_excel(site_crosscheck_path, sheet_name = 'Sheet2', header = 0)
    correctedID_list = {df_correctedID['assessment_id'][i]: df_correctedID['Correct Assessment-ID'][i] for i in range(df_correctedID.shape[0])}
    correctedID_fromFile_list = {df_correctedID_fromFile['Filename'][i]: df_correctedID_fromFile['Correct Assessment-ID'][i] for i in range(df_correctedID_fromFile.shape[0])}
    
    ## cross check assessmnent ID 
    if filename in correctedID_fromFile_list.keys():
        assessment_id = correctedID_fromFile_list[filename]
    else:
        temp_assess_id = df_QRA_Scope[1]
        if temp_assess_id in correctedID_list.keys():
            assessment_id = correctedID_list[temp_assess_id]
        else:
            assessment_id = temp_assess_id

            
    def findSite():
        try:
            site = df_MasterLog[df_MasterLog['QRA ID'] == assessment_id]['Site'].values[0]  ## Should come from the master log (QRR only has high/med risks)
            return site
        except:
            print ('Cannot find site in masterLog')

    site = findSite()
    
    
    QRR_file = str(site) + ' Quality Risk Register.xlsx'
    QRR_file_path = '.' + os.path.sep + 'GSS' + os.path.sep + site + os.path.sep + QRR_file

    ## Read in Register file
    df_QRR_temp = pd.read_excel(QRR_file_path, sheet_name = "Risk Register", header = 1, keep_default_na = False).rename(lambda n: n.strip(), axis = 1)
    
    ## Fix assessment id from Pandaan
    def clean_id_format(data):
        return data.replace('FMEA in','').split(':')[-1].strip()

    if site == 'Pandaan':
        df_QRR_temp['Risk Identification Source'] = df_QRR_temp['Risk Identification Source'].apply(clean_id_format)
   

    QRR_mask = df_QRR_temp['Risk Identification Source'] == assessment_id
    df_QRR = df_QRR_temp[QRR_mask]

    if assessment_id in df_QRR_temp['Risk Identification Source'].tolist():
        print ('QRR extracted')
    else:
        print ('QRR not extracted: assessment_id not found in site QRR')

        
#     df_QRR['Risk ID'] = df_QRR['Risk ID'].apply(check_Numeric)
#     df_QRR = df_QRR[df_QRR['Risk ID'].notnull()]

    ## cross check site and products
    def site_check(data):
        try:
            return sitelist[data]
        except:
            return 'New site: ' + str(data)
    site_output = site_check(site)


    def product_check(data):
        try:
            return productlist[data]
        except:
            return 'New product: ' + str(data)


    ### 1) Populate 'Plan Header'

    ## all columns in Plan Header tab
    PH_columns = [col for col in dict_tab_col['Plan Header'].columns]  
    PH_columns[30] = 'Level 1 Approver_1'
    PH_columns[31] = 'Level 2 Approver_1'


    ### Include all cases of date format
    def getLatestDate(string):    
        if type(string) == datetime:
            latest = string
        else:
            string = re.sub(r'\([^()]*\)', '', string)
            ls = string.replace(' - ',',').replace('and', ',').replace('through', ',').replace('\n', ',').replace('to', ',').replace('&', ',').replace(';', ',').split(',')
            new_ls = []
            for y in ls:
                j = y.replace(' ','').replace('/','').replace('-','').replace('.','').replace('  ','')
                j = j.replace('rd', '').replace('st', '').replace('nd', '').replace('th','')
                new_ls += [j]
            try:
                latest = max([datetime.strptime(d.strip(), '%m%d%Y') for d in new_ls])
            except:
                try:
                    latest = max([datetime.strptime(d.strip(), '%d%m%Y') for d in new_ls])
                except:
                    try:
                        latest = max([datetime.strptime(d.strip(), '%b%d%Y') for d in new_ls])
                    except:
                        try:
                            latest = max([datetime.strptime(d.strip(), '%d%b%Y') for d in new_ls])
                        except:
                            try:
                                latest = max([datetime.strptime(d.strip(), '%B%d%Y') for d in new_ls])
                            except:
                                try:
                                    latest = max([datetime.strptime(d.strip(), '%d%B%Y') for d in new_ls])
                                except:
                                    try:
                                        latest = max([datetime.strptime(d.strip(), '%m%d%y') for d in new_ls])
                                    except:
                                        try:
                                            latest = max([datetime.strptime(d.strip(), '%d%m%y') for d in new_ls])
                                        except:
                                            try:
                                                latest = max([datetime.strptime(d.strip(), '%d%b%y') for d in new_ls])
                                            except:
                                                try:
                                                    latest = max([datetime.strptime(d.strip(), '%b%d%y') for d in new_ls])
                                                except:
                                                    try:
                                                        latest = max([datetime.strptime(d.strip(), '%d%B%y') for d in new_ls])
                                                    except:
                                                        try:
                                                            latest = max([datetime.strptime(d.strip(), '%B%d%y') for d in new_ls])
                                                        except:
                                                            pass


        return str(latest.strftime('%m/%d/%Y'))
    ## Variables needed for population
    try:
        plan_id = assessment_id.replace("QRA", "QRP")
    except:
        plan_id = assessment_id
    line_id = df_QRA_Main.iloc[:, 0].astype(int)
    # assessment_id = df_QRA_Scope[1]
    default_id = 'tstlnstest5040'
    # site = df_MasterLog[df_MasterLog['QRA ID'] == assessment_id]['Site'].values[0]  ## Should come from the master log (QRR only has high/med risks)
    #next_date = getLatestDate(df_QRA_Scope[4])
#     next_date = getLatestDate(df_QRA_Scope[4])
#     print (next_date)

    ## get date from QRR
    date_identified = df_QRR.iloc[0, 9]
    try:
        date_identified = df_QRR.iloc[0, 9].strftime('%m/%d/%Y')
    except:
        pass

    ## try to get date for next_date
    try:
        next_date = getLatestDate(date_identified)
    except:
        next_date = getLatestDate(df_QRA_Scope[4])

    print (next_date)

    if is_empty(df_QRA_Scope[0]):
        title = 'Plan Title Missing'
    else:
        title = str(df_QRA_Scope[0]).strip()
    
    ## Populating the tab
    df_PlanHeader = pd.DataFrame({ 
        'Plan ID': plan_id,
        'Legacy Plan ID': plan_id,
        'Name': title, #df_QRA_Scope[0],  ## 'Quality Risk Assessment Title:'
        'Status': 'Approved',
        'Assessment Type': 'QRM-' + QRA_type,
        'File Name': '',
        'Objectives': '',
        'Scope': '',
        'Background Information and Reference Documents': '',
        'Methodology and Tool Rationale': '',
        'Pre-existing Data': '',
        'Risk Identification': '',
        'Risk Analysis': '',
        'Risk Control': '',
        'References': '',
        'Appendices': '',
        'Organization': site_output,
        'Owner(s)': default_id,
        'Level 1 Approver': default_id,
        'Level 2 Approver': default_id,
        'Valid From': '',
        'Valid Until': '',
        'Frequency': 'Triennial',
        'Assessment Date/Next Scheduled Date': next_date,
        'Start After (Calendar Days)': 1,
        'Due By (Calendar Days)': 90,
        'Default User for All Assessments': 'Yes',
        'Assessor': default_id, # df_QRA_Scope[7],  ## 'Participants/Functional Area:'
        'Reviewers': '',
        'Report Reviewers': '',
        'Level 1 Approver_1': '',
        'Level 2 Approver_1': '',
        'Plan Created By': default_id, # df_QRA_Scope[7],  ## 'Participants/Functional Area:'
        'Comment(s)': '',
    }, index = [0], columns = PH_columns)

    # print (df_PlanHeader)


    # ### 2) Populate 'Assessment'

    # Get all columns in assessment tab
    # dict_tab_col['Assessment'].columns


    def Yes_No_transform(data):
        if re.findall('yes|si', str(data).lower()):
            new = 'Accept'
        elif 'no' in str(data).lower():
            new = 'Mitigate'
        else:
            new = data
        return new

    def Yes_No_Blank_transform(data):
        if re.findall('yes|si|nan|na', str(data).lower().replace('/','')):
            new = 'Accept'
        elif 'no' in str(data).lower():
            new = 'Mitigate'
        elif not data:
            new = 'Accept'
        else:
            new = str(data)
        return new

    def blank_score(data):
        try:
            data = int(data)
            return data
        except:
            data = str(data).strip()
            if not data:
                return '2'
            if 'a' in data.lower():
                return '2'
            else:
                return data    
    
    def to_NA(data):
        if not str(data).strip():
            return 'NA'
        elif str(data).strip().lower() == 'nan':
            return 'NA'
        else:
            return data

    def getLatestDate_CAPA(date):
        # print(date)
        # import pdb; pdb.set_trace()
        if not date or pd.isnull(date):
            return date
        elif (type(date) in [datetime, pd.Timestamp]):
            return str(date.strftime('%m/%d/%Y'))
        elif date.replace('/','') == 'NA':
            return ''
        else:
            # date = re.sub(r'\([^()]*\)', '', date)
            latest = None
            ls = [a for a in date.split('\n') if a.strip()]
            new_ls = []
            for y in ls:
                if '.' in y:
                    dot = y.index('.')
                    yy = y[dot+1:].strip()
                else:
                    yy = y.strip()
                j = yy.replace(' ','').replace('/','').replace('-','').replace('.','').replace('  ','')
                new_ls += [j]

            try:
                latest = max([datetime.strptime(d.strip(), '%d%b%Y') for d in new_ls])
            except:
                try:
                    latest = max([datetime.strptime(d.strip(), '%d%m%Y') for d in new_ls])
                except:
                    try:
                        latest = max([datetime.strptime(d.strip(), '%b%d%Y') for d in new_ls])
                    except:
                        try:
                            latest = max([datetime.strptime(d.strip(), '%m%d%Y') for d in new_ls])
                        except:
                            try:
                                latest = max([datetime.strptime(d.strip(), '%B%d%Y') for d in new_ls])
                            except:
                                try:
                                    latest = max([datetime.strptime(d.strip(), '%d%B%Y') for d in new_ls])
                                except:
                                    try:
                                        latest = max([datetime.strptime(d.strip(), '%m%d%y') for d in new_ls])
                                    except:
                                        try:
                                            latest = max([datetime.strptime(d.strip(), '%d%m%y') for d in new_ls])
                                        except:
                                            try:
                                                latest = max([datetime.strptime(d.strip(), '%d%b%y') for d in new_ls])
                                            except:
                                                try:
                                                    latest = max([datetime.strptime(d.strip(), '%b%d%y') for d in new_ls])
                                                except:
                                                    try:
                                                        latest = max([datetime.strptime(d.strip(), '%d%B%y') for d in new_ls])
                                                    except:
                                                        try:
                                                            latest = max([datetime.strptime(d.strip(), '%B%d%y') for d in new_ls])
                                                        except:
                                                            # PT
                                                            try:
                                                                dates = date.split("\n")
                                                                new_ls = []
                                                                # import pdb; pdb.set_trace()
                                                                for d in dates:
                                                                    d_temp = d.split(" ")[1:]
                                                                    d_temp = "".join(d_temp)
                                                                    new_ls += [datetime.strptime("".join(d_temp), '%d%b%y')]
                                                                latest = max(new_ls)
                                                            except:
                                                                pass
            if (latest is None):
                # if date does not fall into above condition, return the original format.
                return date

            return str(latest.strftime('%m/%d/%Y'))
    
    ## Empty tab from template to be populated
    df_Assessment = dict_tab_col['Assessment'] 

    ## Populate each column
    df_Assessment['Line ID'] = df_QRA_Main.iloc[:, 0].astype(int)  # ['index']
    df_Assessment['File Name'] = '' 
    df_Assessment['Status'] = 'Approved' 
    df_Assessment['Site'] = site_output 
    # df_Assessment['Product'] = df_QRA_Main.iloc[:, 1].apply(product_check)   # ['Product']
    df_Assessment['Category'] = '' 
    df_Assessment['Parameter/Input'] = ''
    df_Assessment['Parameter during Development OR at Sending Site'] = ''
    df_Assessment['Proposed Parameter at Receiving Site'] = '' 
    df_Assessment['Process/System'] = df_QRA_Main.iloc[:, 2]    # ['Process/System']
    df_Assessment['Process Step/ Function'] = df_QRA_Main.iloc[:, 3]   # ['Process Step/ Function'] 
    df_Assessment['Change request'] = '' 
    df_Assessment['Req/Char/Spec'] = df_QRA_Main.iloc[:, 4]   # ['Req/Char/Spec (This column is optional)'] 
    df_Assessment['Harm'] = df_QRA_Main.iloc[:, 6]  # ['Harm']
    df_Assessment['Hazard'] = df_QRA_Main.iloc[:, 5]  # ['Hazard'] 
    df_Assessment['Hazardous Situation'] = df_QRA_Main.iloc[:, 8]  # ['Hazardous Situation'] 
    df_Assessment['Failure Effect'] = '' 
    df_Assessment['Failure Mode'] = ''
    df_Assessment['Potential Cause  of Failure'] = '' 
    df_Assessment['What-If'] = '' 
    df_Assessment['What-If Response'] = ''
    df_Assessment['Assessor'] = '' 
    df_Assessment['Collaborator'] = '' 
    df_Assessment['Comments'] = df_QRA_Main.iloc[:, 31]   # ['Comments'] 
    df_Assessment['Score'] = df_QRA_Main.iloc[:, 7].apply(blank_score)   # ['Sev'] 
    df_Assessment['Scoring Rationale'] = 'Per GP2.40.1'
    df_Assessment['Comments.1'] = '' 
    df_Assessment['Score.1'] = df_QRA_Main.iloc[:, 15].apply(blank_score)   # ['Occ'] 
    df_Assessment['Scoring Rationale.1'] = df_QRA_Main.iloc[:, 16]   # ['Rationale for Probability of Occurrence  Score'] 
    df_Assessment['Comments.2'] = '' 
    df_Assessment['Score.2'] = df_QRA_Main.iloc[:, 20].apply(blank_score)   # ['Det']
    df_Assessment['Scoring Rationale.2'] = df_QRA_Main.iloc[:, 21]   # ['Rationale for Likelihood of Detection'] 
    df_Assessment['Comments.3'] = '' 
    df_Assessment['Score.3'] = df_QRA_Main.iloc[:, 26] #['Sev']  ## !!!Not the same 'sev' column this one is under another header!!!
    df_Assessment['Scoring'] = '' 
    df_Assessment['Comments.4'] = ''
    df_Assessment['Score.4'] = df_QRA_Main.iloc[:, 27]    # ['Occ']  ## !!!Not the same 'occ' column this one is under another header!!!
    df_Assessment['Justification'] = ''
    df_Assessment['Comments.5'] = '' 
    df_Assessment['Score.5'] = df_QRA_Main.iloc[:, 29]  # ['Det']  ## !!!Not the same 'det' column this one is under another header!!! 
    df_Assessment['Justification.1'] = ''
    df_Assessment['Comments.6'] = '' 
    df_Assessment['Score.6'] = '' 
    df_Assessment['Scoring Rationale.3'] = '' 
    df_Assessment['Comments.7'] = ''
    df_Assessment['Score.7'] = ''
    df_Assessment['Scoring Rationale.4'] = ''
    df_Assessment['Comments.8'] = ''
    df_Assessment['Score.8'] = '' 
    df_Assessment['Scoring Rationale.5'] = ''
    df_Assessment['Comments.9'] = '' 
    df_Assessment['Risk Description'] = ''  ## Register file ['Rist Identified']
    df_Assessment['Inherent  Risk Control Decision?'] = df_QRA_Main.iloc[:, 23].apply(Yes_No_Blank_transform)# ['Risk Accepted?'] 
    df_Assessment['Justification for Inherent Risk Acceptance'] = df_QRA_Main.iloc[:, 24]  # ['Justification for Acceptance']
    df_Assessment['Residual  Risk Control Decision?'] = ''  ## Register file  ['Residual Risk Accepted']
    df_Assessment['Justification for Residual  Risk Acceptence'] = '' 
    df_Assessment['Go/No Go?'] = ''
    df_Assessment['Go/No Go Rationale'] = '' 
    df_Assessment['Clarifying Rationale'] = ''
    df_Assessment['GAP Impact'] = ''
    df_Assessment['GAP Impact Reason'] = ''
    df_Assessment['GAP Assessment Action'] = ''
    df_Assessment['GAP Assessment Comments'] = ''


    ## cols with constant values
    df_Assessment['Date Identified'] = next_date
    df_Assessment['Assessment ID'] = assessment_id 
    df_Assessment['Plan ID'] = plan_id 
    df_Assessment['Assessment Title'] = df_QRA_Scope[0] 
    df_Assessment['Legacy Assessment ID'] = df_QRA_Scope[1] 
    df_Assessment['Product'] = 'All Products'#df_QRA_Main.iloc[:, 1].apply(product_check)   # ['Product']

    # df_Assessment.head(3)


    ## Extract matching rows from QRR
    # QRR_mask = df_QRR_temp['Risk Identification Source'] == assessment_id
    # df_QRR = df_QRR_temp[QRR_mask]
    # df_QRR.info()


    ## Get the columns asked for in template from Register file
    df_QRR_for_Assessment = df_QRR[['Risk Identification Source', 'Risk ID', 'Risk Identified', 'Residual Risk Accepted?']].rename({"Risk Identification Source": "Assessment ID", "Risk ID": "Line ID", "Risk Identified": "_Risk Description", "Residual Risk Accepted?": "_Residual  Risk Control Decision?"}, axis=1)

    df_QRR_for_Assessment['_Residual  Risk Control Decision?'] = df_QRR_for_Assessment['_Residual  Risk Control Decision?'].apply(Yes_No_transform)
    df_QRR_for_Assessment['Line ID'] = df_QRR_for_Assessment['Line ID'].astype(int)
    # df_QRR_for_Assessment.head(3)


    ## Merge columns from Register to Assessment tab
    df_Assessment_merged = df_Assessment.merge(df_QRR_for_Assessment, how = "left", on = ["Assessment ID", "Line ID"])
    # df_Assessment_merged.info()


    ## Populating columns asking for input from Register
    df_Assessment_merged['Risk Description'] = df_Assessment_merged['_Risk Description']  ## Register file ['Rist Identified']
    df_Assessment_merged['Residual  Risk Control Decision?'] = df_Assessment_merged['_Residual  Risk Control Decision?']  ## Register file  ['Residual Risk Accepted']
    # df_Assessment_merged.info()


    ## Remove duplicate columns from temporary df of Register
    df_Assessment_final = df_Assessment_merged.drop(['_Risk Description', '_Residual  Risk Control Decision?'], axis = 1)
    df_Assessment_final['Justification for Residual  Risk Acceptence'] = df_Assessment_final['Residual  Risk Control Decision?']
    for i in df_Assessment_final.index:
        temp_data = df_Assessment_final['Justification for Inherent Risk Acceptance'][i]
        if not str(temp_data).strip() or str(temp_data).strip().lower() == 'nan':
            df_Assessment_final['Justification for Inherent Risk Acceptance'][i] = df_Assessment_final['Inherent  Risk Control Decision?'][i]



    # df_Assessment_final.head(3)


    ### 3) Populate Control Rating

    df_CtrlR = dict_tab_col['Control Rating']
    # df_CtrlR.columns


    df_CtrlR['LINE ID'] = line_id
    df_CtrlR['Assessment ID'] = assessment_id
    df_CtrlR['Control'] = ''
    df_CtrlR['Purpose'] = ''
    df_CtrlR['Control Reference'] = ''
    df_CtrlR['Date'] = ''
    df_CtrlR['Control Status'] = 'Active'


    ## Create the weird columns in PHA (multiple rows for each line_id)
    dict_purpose = {1: 'Prevention – Design', 2: 'Prevention – Quality Systems', 3: 'Prevention – Qualification', 4: 'Detection'}

    def sub_df(df_source, col, col_int, marker):
        df = pd.DataFrame(columns = ['LINE ID', '_marker', col, '_Purpose'])
        df['LINE ID'] = line_id
        df['_marker'] = marker
        df[col] = df_source.iloc[:, col_int]
        df['_Purpose'] = dict_purpose[marker]
        return df

    ## combine all sub dfs for Control 
    df_CtrlR_Ctrl_1 = sub_df(df_QRA_Main, '_Control', 9, 1)
    df_CtrlR_Ctrl_2 = sub_df(df_QRA_Main, '_Control', 11, 2)
    df_CtrlR_Ctrl_3 = sub_df(df_QRA_Main, '_Control', 13, 3)
    df_CtrlR_Ctrl_4 = sub_df(df_QRA_Main, '_Control', 18, 4)

    df_CtrlR_Ctrl = pd.concat([df_CtrlR_Ctrl_1, df_CtrlR_Ctrl_2, df_CtrlR_Ctrl_3, df_CtrlR_Ctrl_4])

    ## combine all sub dfs for Control Ref 
    df_CtrlR_Ref_1 = sub_df(df_QRA_Main, '_Control Reference', 10, 1)
    df_CtrlR_Ref_2 = sub_df(df_QRA_Main, '_Control Reference', 12, 2)
    df_CtrlR_Ref_3 = sub_df(df_QRA_Main, '_Control Reference', 14, 3)
    df_CtrlR_Ref_4 = sub_df(df_QRA_Main, '_Control Reference', 19, 4)

    df_CtrlR_Ref = pd.concat([df_CtrlR_Ref_1, df_CtrlR_Ref_2, df_CtrlR_Ref_3, df_CtrlR_Ref_4])
    df_CtrlR_Ref = df_CtrlR_Ref.drop(['_Purpose'], axis = 1)

    # df_CtrlR_Ref.head()


    ## Now combine those columns back to the original df for the tab
    ## First, add in the Control col
    df_CtrlR_merged1 = df_CtrlR.merge(df_CtrlR_Ctrl, on = ['LINE ID'])
    df_CtrlR_merged1['Control'] = df_CtrlR_merged1['_Control']
    df_CtrlR_merged1['Purpose'] = df_CtrlR_merged1['_Purpose']

    ## Second, add in the Reference col
    df_CtrlR_merged2 = df_CtrlR_merged1.merge(df_CtrlR_Ref, on = ['LINE ID', '_marker'])
    df_CtrlR_merged2['Control Reference'] = df_CtrlR_merged2['_Control Reference']
    df_CtrlR_final = df_CtrlR_merged2.drop(['_marker', '_Control', '_Control Reference', '_Purpose'], axis = 1)

    # df_CtrlR_final.head(12)


    ### 4) Populate Mitigating Actions

    df_MAct = dict_tab_col['Mitigating Actions']
    

    df_MAct['Line ID'] = df_QRR.iloc[:, 4].astype(int)
    df_MAct['Assessment ID'] = assessment_id
    df_MAct['Mitigation Actions'] = df_QRR.iloc[:, 12].apply(to_NA)
    df_MAct['Mitigation Priority'] = df_QRR.iloc[:, 14].apply(to_NA)
    df_MAct['Date Proposed'] = next_date  ## !!! Double check: from QRA file Scope tab
    df_MAct['CAPA ID'] = df_QRR.iloc[:, 17].apply(to_NA).apply(lambda x: str(x)[-20:])  ## 20 character limit in metric stream
    df_MAct['CAPA Owner'] = df_QRR.iloc[:, 16].apply(to_NA).apply(lambda x: str(x)[-20:])
    df_MAct['Function'] = df_QRR.iloc[:, 15].apply(to_NA)
    # df_MAct['CAPA Due Date'] = df_QRR.iloc[:, 18].apply(to_NA).apply(lambda x: str(x.strftime('%m/%d/%Y')) if (type(x) == datetime) else x)
    df_MAct['CAPA Due Date'] = df_QRR.iloc[:, 18].apply(to_NA).apply(getLatestDate_CAPA)
    df_MAct['Status'] = df_QRR.iloc[:, 20].apply(to_NA)

    # df_MAct.head()

    ## Cross check QRR and QRA line id; Set line id to int
    df_lineid_check = df_Assessment[['Line ID']].astype(int)
    df_MAct_final = df_MAct.merge(df_lineid_check, how  = 'inner', on = 'Line ID')
    # df_MAct_final = df_MAct_final.drop_duplicates()

    ## New rule: Cross check line ID with Assessment tab mitigate rows
    mitigate_list = df_Assessment_final[df_Assessment_final['Inherent  Risk Control Decision?'] == 'Mitigate']['Line ID'].tolist()
    Add_to_MAct = []
    for lineid in mitigate_list:
        if lineid not in df_MAct_final['Line ID'].tolist():
            Add_to_MAct += [lineid]

    df_MAct_add = pd.DataFrame(columns = dict_tab_col['Mitigating Actions'].columns)
    df_MAct_add['Line ID'] = Add_to_MAct
    df_MAct_add['Assessment ID'] = assessment_id
    df_MAct_add['Mitigation Actions'] = 'NA'
    df_MAct_add['Mitigation Priority'] = 'NA'
    df_MAct_add['Date Proposed'] = ''
    df_MAct_add['CAPA ID'] = 'NA'
    df_MAct_add['CAPA Owner'] = 'NA'
    df_MAct_add['Function'] = 'NA'
    df_MAct_add['CAPA Due Date'] = ''
    df_MAct_add['Status'] = 'NA'

    df_MAct_final_checked = pd.concat([df_MAct_final, df_MAct_add], ignore_index=True).drop_duplicates()


    ### 5) Populate QRM Risk Register

    df_QRMRR = dict_tab_col['QRM Risk Register']
    # df_QRMRR.columns


    def OperatingUnit_Change(data):
        if str(data).strip().lower() == 'small molecule':
            new = 'Global Pharmaceutical Operations'
        elif str(data).strip().lower() == 'large molecule':
            new = 'Global Bio/Sterile Operations'
        elif str(data).strip().lower() == 'large molecule (vaccines)':
            new = 'Vaccines Operations'
        else:
            new = data
        return new

    ## New rules for operating unit 
    OperateUnit_path = '.' + os.path.sep + 'Details' + os.path.sep + 'QRM Sites- Operating Unit and Region _Cross reference.xlsx'
    df_OperateUnit = pd.read_excel(OperateUnit_path, header = 0)
    site_OperateUnit = {df_OperateUnit['Site'][i]: df_OperateUnit['Operating Unit'][i] for i in range(df_OperateUnit.shape[0])}
    site_Region = {df_OperateUnit['Site'][i]: df_OperateUnit['Region'][i] for i in range(df_OperateUnit.shape[0])}

    

    df_QRMRR['Line ID'] = df_QRR.iloc[:, 4].astype(int)
    df_QRMRR['Risk Category'] = df_QRR.iloc[:, 6]
    df_QRMRR['Unit Operation Category'] = df_QRR.iloc[:, 7]
    df_QRMRR['Operating Unit'] = df_QRR.iloc[:, 0].apply(OperatingUnit_Change) # site_OperateUnit[site]
    df_QRMRR['Region/Group'] =  df_QRR.iloc[:, 1]  # site_Region[site]
    df_QRMRR['Risk Owner'] = default_id # df_QRA_Scope[7]
    df_QRMRR['Comments'] = df_QRR.iloc[:, 24]
    df_QRMRR['Assessment ID'] = assessment_id

    # df_QRMRR.head()

    ## Cross check QRR and QRA line id
    df_QRMRR_final = df_QRMRR.merge(df_lineid_check, how  = 'inner', on = 'Line ID')
    # df_QRMRR_final = df_QRMRR_final.drop_duplicates()

    ## New rule: Cross check line ID with Assessment tab mitigate rows
    Add_to_QRMRR = []
    for lineid in mitigate_list:
        if lineid not in df_QRMRR_final['Line ID'].tolist():
            Add_to_QRMRR += [lineid]

    df_QRMRR_add = pd.DataFrame(columns = dict_tab_col['QRM Risk Register'].columns)
    df_QRMRR_add['Line ID'] = Add_to_QRMRR
    df_QRMRR_add['Risk Category'] = 'NA'
    df_QRMRR_add['Unit Operation Category'] = 'NA'
    df_QRMRR_add['Operating Unit'] = 'Global Pharmaceutical Operations' # site_OperateUnit[site]
    df_QRMRR_add['Region/Group'] = 'Small Molecule (AP)' # site_Region[site]
    df_QRMRR_add['Risk Owner'] = default_id
    df_QRMRR_add['Comments'] = ''
    df_QRMRR_add['Assessment ID'] = assessment_id

    df_QRMRR_final_checked = pd.concat([df_QRMRR_final, df_QRMRR_add], ignore_index=True).drop_duplicates()

    
    ### 6) Populate Report Details
    Report_columns = dict_tab_col['Report Details'].columns
    df_Report = pd.DataFrame({ 
        'Assessment ID': assessment_id,
        'SQRMC Risk Decisions': 'Yes',
        'Confidentiality Statement for the Assessment': '', 
        'Objective': '', 
        'Scope': '',
        'Description of product or process or system': '', 
        'Methodology': '',
        'Risk Overview': '', 
        'Quality Risk Assessment Output': '', 
        'Risk Review': '',
        'Conclusion': '',
    }, index = [0], columns = Report_columns)


    ###### !!!!!! REMOVE LATER !!!!!!########
    def Temporary_default(data, default):
        if not data:
            return default
        elif str(data).lower().replace('/','') == 'na':
            return default
        else:
            return data

    def Temporary_id(data):
        return str(data) + 'jp3'


    df_PlanHeader['Organization'] = 'Brinny'
    df_PlanHeader['Plan ID'] = df_PlanHeader['Plan ID'].apply(Temporary_id)
    df_PlanHeader['Legacy Plan ID'] = df_PlanHeader['Legacy Plan ID'].apply(Temporary_id)
    df_Assessment_final['Plan ID'] = df_Assessment_final['Plan ID'].apply(Temporary_id)
    df_Assessment_final['Product'] = 'All Products'
    df_Assessment_final['Assessment ID'] = df_Assessment_final['Assessment ID'].apply(Temporary_id)
    df_CtrlR_final['Assessment ID'] = df_CtrlR_final['Assessment ID'].apply(Temporary_id)
    df_MAct_final_checked['Assessment ID'] = df_MAct_final_checked['Assessment ID'].apply(Temporary_id)
    df_QRMRR_final_checked['Assessment ID'] = df_QRMRR_final_checked['Assessment ID'].apply(Temporary_id)
    df_Report['Assessment ID'] = df_Report['Assessment ID'].apply(Temporary_id)

    df_QRMRR_final_checked['Risk Category'] = df_QRMRR_final_checked['Risk Category'].apply(lambda x: Temporary_default(x, 'Reprocessing'))
    df_QRMRR_final_checked['Unit Operation Category'] = df_QRMRR_final_checked['Unit Operation Category'].apply(lambda x: Temporary_default(x,'Component/Material Prep'))


    
    ### Save every tab to excel, including the empty ones

    ## Create empty tabs
    # df_Assessment = dict_tab_col['Assessment']
    # dict_tab_col.keys()

    ## Reset the header to the 2-level one from template

    doubleHeader_PlanHeader = pd.read_excel(QRA_temp_path, sheet_name = 'Plan Header', header = [0, 1], index_col=None).columns
    df_PlanHeader_doubleH = df_PlanHeader.reindex(columns = df_PlanHeader.columns)
    df_PlanHeader_doubleH.columns = doubleHeader_PlanHeader

    doubleHeader_Assessment = pd.read_excel(QRA_temp_path, sheet_name = 'Assessment', header = [0, 1], index_col=None).columns
    df_Assessment_doubleH = df_Assessment_final.reindex(columns = df_Assessment_final.columns)
    df_Assessment_doubleH.columns = doubleHeader_Assessment


    ## Save every df to a worksheet
    # from datetime import datetime
    now = datetime.now()
    month, day, year = now.strftime('%m'), now.strftime('%d'), now.strftime('%Y')
    excel_name = 'MS_' + all_QRA_types[QRA_type] + '_' + assessment_id + '.xlsx'
    excel_output = '.' + os.path.sep + 'Output' + os.path.sep + excel_name
    # excel_output = 'Py_QRA_' + all_QRA_types[QRA_type] + '_demo_' + month + day + year + '.xlsx'

    
    
    writer = pd.ExcelWriter(excel_output)
    pd.DataFrame(columns = ['Instruction']).to_excel(writer, sheet_name = 'Instruction', index = False)
    df_PlanHeader_doubleH.to_excel(writer, sheet_name = 'Plan Header')
    dict_tab_col['Plan Assessments'].to_excel(writer, sheet_name = 'Plan Assessments', index = False)
    dict_tab_col['Plan-QRM Team Details'].to_excel(writer, sheet_name = 'Plan-QRM Team Details', index = False)
    df_Assessment_doubleH.to_excel(writer, sheet_name = 'Assessment')
    df_CtrlR_final.to_excel(writer, sheet_name = 'Control Rating', index = False)
    df_MAct_final_checked.to_excel(writer, sheet_name = 'Mitigating Actions', index = False)
    df_Report.to_excel(writer, sheet_name = 'Report Details', index = False)
    dict_tab_col['Report Reviewers - Approvers'].to_excel(writer, sheet_name = 'Report Reviewers - Approvers', index = False)
    dict_tab_col['Report QRA Participants '].to_excel(writer, sheet_name = 'Report QRA Participants ', index = False)
    df_QRMRR_final_checked.to_excel(writer, sheet_name = 'QRM Risk Register', index = False)

    writer.save()

    ## Use openpyxl to remove rows
    wb = load_workbook(excel_output)
    wb['Plan Header'].delete_rows(3,1)
    wb['Assessment'].delete_rows(3,1)
    wb.save(excel_output)

    ## Get rid of index col    
    wb = load_workbook(excel_output) 
    ws = wb.worksheets[1] ## select 2nd sheet -- Plan Header

    ## gets a list of cell ranges, each one is a openpyxl object
    merged_cells_range = ws.merged_cells.ranges
    ## bounds returns a tuple of 4 numbers, so this makes a list of tuples.  
    ## Each tuple is of the form (A,B,C,D) where A is the first col, B is upper row, C is last col and D is lower row
    ## i.e. (5,1,18,1) is a merged cell from E1:R1
    merged_bounds = [x.bounds for x in merged_cells_range]
    ## reverse the list because we need to unmerge from the back to maintain the correct indices
    merged_bounds.reverse()

    ## loop through the list of bounds and unmerge the cells
    for bound in merged_bounds:
        ws.unmerge_cells(start_row=bound[1], start_column=bound[0], end_row=bound[3], end_column=bound[2])

    ## delete the first column
    ws.delete_cols(1)

    ## loop back through the list of bounds again and merge the cells,
    ## this time subtracting 1 from the start and end col because we deleted the first column so everything shifted
    for bound in merged_bounds:
        ws.merge_cells(start_row=bound[1], start_column=bound[0]-1, end_row=bound[3], end_column=bound[2]-1)

    ## Do it all again for the Assessment tab
    ws = wb.worksheets[4]
    merged_cells_range = ws.merged_cells.ranges
    merged_bounds = [x.bounds for x in merged_cells_range]
    merged_bounds.reverse()
    for bound in merged_bounds:
        # print(bound)
        ws.unmerge_cells(start_row=bound[1], start_column=bound[0], end_row=bound[3], end_column=bound[2])
    ws.delete_cols(1)

    for bound in merged_bounds:
        # print(bound)
        ws.merge_cells(start_row=bound[1], start_column=bound[0]-1, end_row=bound[3], end_column=bound[2]-1)

    wb.save(excel_output)
    
    
    print ('output file created', QRA_type)
    return (site, assessment_id)
