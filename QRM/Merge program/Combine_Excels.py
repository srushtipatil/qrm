import sys
import os
from copy import copy
import pandas as pd
import re,shutil,numpy as np
from openpyxl import load_workbook,Workbook

#for single line headers
def to_separate_excels_single(excel_sheet_name):
	frames = [x.parse(excel_sheet_name, header=None,index_col=None) for x in excels]

	# delete the first row for all frames except the first
	# i.e. remove the header row -- assumes it's the first
	frames[1:] = [df[1:] for df in frames[1:]]

	# concatenate them..
	combined = pd.concat(frames)

	# write it out
	combined.to_excel("merged\\"+excel_sheet_name+".xlsx", header=False, index=False,  sheet_name=excel_sheet_name)

#for multi line headers
def to_separate_excels_multi_line(excel_sheet_name):
	frames = [x.parse(excel_sheet_name, header=None,index_col=None) for x in excels]

	# delete the header row for all frames except the first
	# i.e. remove the header row -- assumes it's the first
	frames[1:] = [df[2:] for df in frames[1:]]

	# concatenate them..
	combined = pd.concat(frames)

	# write it out
	combined.to_excel("merged\\"+excel_sheet_name+".xlsx", header=False, index=False,  sheet_name=excel_sheet_name)

	
# filenames
os.chdir("C:\\Users\\sinhanis\\Documents\\QRM\\Python Scripts\\Upload\\Set1\\")

all_excels = []
for files in os.listdir():
    if files.endswith(".xlsx") :
        if re.match('MS_', files):
            print(files)
            all_excels.append(files)
        

#Required Lists Declaration:
excel_names = []
next_set = []
exceptional_files = []
	
while all_excels:
	len(all_excels)
	i=0
	length = 0
	while(i < len(all_excels)):
		#print(all_excels[i])
		Assessment = pd.read_excel(all_excels[i],sheet_name='Assessment')
		#print(len(Assessment))
		
		if len(all_excels)==1:
			shutil.move(all_excels[i],"to_be_uploaded")
			print("----!!!!!----!!!!----!!!!---PROCESS COMPLETED---!!!!----!!!!----!!!!!----")
			exit()
		
		if(len(Assessment)>500):
			exceptional_files.append(all_excels[i])
			shutil.move(all_excels[i],"to_be_uploaded")
			all_excels.remove(all_excels[i])
		else:   
			length = length + len(Assessment)
			if(length<500):
				excel_names.append(all_excels[i])
				all_excels.remove(all_excels[i])
			elif(length>500):
				next_set.append(all_excels[i])
		i=i+1
		
	print(excel_names)
	print(next_set)


	# In[384]:


	# read them in
	#print(excel_names)
	excels = [pd.ExcelFile(name) for name in excel_names]

	#Create excels for each sheet merge
	to_separate_excels_single('Instruction')
	to_separate_excels_multi_line('Plan Header')
	to_separate_excels_single('Plan Assessments')
	to_separate_excels_single('Plan-QRM Team Details')
	to_separate_excels_multi_line('Assessment')
	to_separate_excels_single('Control Rating')
	to_separate_excels_single('Mitigating Actions')
	to_separate_excels_single('Report Details')
	to_separate_excels_single('Report Reviewers - Approvers')
	to_separate_excels_single('Report QRA Participants ')
	to_separate_excels_single('QRM Risk Register')


	#Read all the excels created
	df1=pd.read_excel('merged\\Instruction.xlsx','Instruction',keep_default_na = False)
	df2=pd.read_excel('merged\\Plan Header.xlsx','Plan Header',header=[0, 1],keep_default_na = False)
	df3=pd.read_excel('merged\\Plan Assessments.xlsx','Plan Assessments',keep_default_na = False)
	df4=pd.read_excel('merged\\Plan-QRM Team Details.xlsx','Plan-QRM Team Details',keep_default_na = False)
	df5=pd.read_excel('merged\\Assessment.xlsx','Assessment',header=[0, 1],keep_default_na = False)
	df6=pd.read_excel('merged\\Control Rating.xlsx','Control Rating',keep_default_na = False)
	df7=pd.read_excel('merged\\Mitigating Actions.xlsx','Mitigating Actions',inplace=True,keep_default_na = False)
	df8=pd.read_excel('merged\\Report Details.xlsx','Report Details',keep_default_na = False)
	df9=pd.read_excel('merged\\Report Reviewers - Approvers.xlsx','Report Reviewers - Approvers',keep_default_na = False)
	df10=pd.read_excel('merged\\Report QRA Participants .xlsx','Report QRA Participants ',keep_default_na = False)
	df11=pd.read_excel('merged\\QRM Risk Register.xlsx','QRM Risk Register',keep_default_na = False)
	#and so on to read different excel

	#Creating name for merged file
	j=0
	filename=''
	while(j<len(excel_names)):
		#print(excel_names[j])
		filename= filename + excel_names[j]
		j=j+1

	filename = filename.replace(".xlsx","__")
	print(excel_names[j-1])

	excel_output = 'to_be_uploaded\\'+'MS_Merged_'+ excel_names[j-1]

	#Write to a new excel where all files will be merged as single file
	with pd.ExcelWriter(excel_output) as writer:
		df1.to_excel(writer,sheet_name='Instruction',index=False)
		df2.to_excel(writer,sheet_name='Plan Header')
		df3.to_excel(writer,sheet_name='Plan Assessments',index=False)
		df4.to_excel(writer,sheet_name='Plan-QRM Team Details',index=False)
		df5.to_excel(writer,sheet_name='Assessment')
		df6.to_excel(writer,sheet_name='Control Rating',index=False)
		df7.to_excel(writer,sheet_name='Mitigating Actions',index=False)
		df8.to_excel(writer,sheet_name='Report Details',index=False)
		df9.to_excel(writer,sheet_name='Report Reviewers - Approvers',index=False)
		df10.to_excel(writer,sheet_name='Report QRA Participants ',index=False)
		df11.to_excel(writer,sheet_name='QRM Risk Register',index=False)
		writer.save()
		
	#and so on for different excel files to different tabs

	#To get rid of index column
	wb = load_workbook(excel_output)
	wb['Plan Header'].delete_rows(3,1)
	wb['Assessment'].delete_rows(3,1)
	wb.save(excel_output)


	## Get rid of index col 
	## Get rid of index col    
	wb = load_workbook(excel_output) 
	ws = wb.worksheets[1] ## select 2nd sheet -- Plan Header

	## gets a list of cell ranges, each one is a openpyxl object
	merged_cells_range = ws.merged_cells.ranges
	## bounds returns a tuple of 4 numbers, so this makes a list of tuples.  
	## Each tuple is of the form (A,B,C,D) where A is the first col, B is upper row, C is last col and D is lower row
	## i.e. (5,1,18,1) is a merged cell from E1:R1
	merged_bounds = [x.bounds for x in merged_cells_range]
	## reverse the list because we need to unmerge from the back to maintain the correct indices
	merged_bounds.reverse()

	## loop through the list of bounds and unmerge the cells
	for bound in merged_bounds:
		ws.unmerge_cells(start_row=bound[1], start_column=bound[0], end_row=bound[3], end_column=bound[2])

	## delete the first column
	ws.delete_cols(1)

	## loop back through the list of bounds again and merge the cells,
	## this time subtracting 1 from the start and end col because we deleted the first column so everything shifted
	for bound in merged_bounds:
		ws.merge_cells(start_row=bound[1], start_column=bound[0]-1, end_row=bound[3], end_column=bound[2]-1)

	## Do it all again for the Assessment tab
	ws = wb.worksheets[4]
	merged_cells_range = ws.merged_cells.ranges
	merged_bounds = [x.bounds for x in merged_cells_range]
	merged_bounds.reverse()
	for bound in merged_bounds:
		# print(bound)
		ws.unmerge_cells(start_row=bound[1], start_column=bound[0], end_row=bound[3], end_column=bound[2])
	ws.delete_cols(1)

	for bound in merged_bounds:
		# print(bound)
		ws.merge_cells(start_row=bound[1], start_column=bound[0]-1, end_row=bound[3], end_column=bound[2]-1)

	wb.save(excel_output) 
	print("----------------------Merge Completed --------------------------")
	

	#Move the files to completed folder
	j=0
	print("------------------Now Moving Files to Completed---------------------")
	while(j<len(excel_names)):
		print(excel_names[j])
		shutil.move(excel_names[j],"Completed")
		j=j+1
	print("----------------------- Sucessfully Moved --------------------------")
	excel_names = []
	next_set = []
	
	
print("----!!!!!----!!!!----!!!!---PROCESS COMPLETED---!!!!----!!!!----!!!!!----")
	