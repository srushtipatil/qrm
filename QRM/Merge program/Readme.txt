Create 3 subfolders in the folder of the output files as:
1.merged
2.Completed
3.to_be_uploaded

Output_files_folder
|----merged
|----Completed
|----to_be_uploaded

merged - contains every sheet of the mergeable data merged as a separate excel
         Ex: Plan Header sheet of every excel that are in the mergeable list will merged and kept as Plan Header.xlsx
	 This folder will only be used for process of merge this can be later discarded

Completed - This folder contains all the original excels that have merged and kept in another folder

to_be_uploaded - This folder contains all the merged files and also contains the files that have more than 500 lines in                     Assesment sheet
 

Run the program after these folder are created.