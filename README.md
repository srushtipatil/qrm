# README #

Thir repository contains all the files,packages and drivers required for running the codes written in python.
# The programs work on Python3 #


### What is this repository for? ###

* QRM 
* The folder QRM contains all the files required for the successful run of the QRM_main which is the main file for getting the       output files for the processing.
* The other folder contains the packages and drivers required for the successful run of the metricstream selenium automation. 

### How to install the packages ###
Go to the folder where all packages are present.
Do cd All Required Packages and driver to go to the location of the packages

Install required packages using the following commands

* pip install robotframework-3.1.1-py2.py3-none-any.whl
* pip install selenium-3.141.0-py2.py3-none-any.whl
* go to pyautoit-master location (do it by using cd pyautoit-master)
  python setup.py install
* go to times-0.7 location (do it by using cd times-0.7)
  python setup.py install
  
  
* Add the location of cromedriver to the environment variables

### QRM Folder ###

* The Automation Folder conatins the selenium automation program
* To use Automation program change the locaioon of the files to be uploaded and run the program using 
  python qrm-automation.py

* The Merge program Folder contains the merge program for combining multiple excels into a single excel
* Run the program by changing the location of the files to be merged
  python combine-excels.py
* To run ipynb file open jupyter notebook and open the file using the same and click on run in it.
  
The other details for merge are present in the respective folder
  
  
* The Details Folder contains all supporting excel files required for creating output files for further processing in             metriccstream.

* The following 4 files are used for creating 4 different types of output files present in QRM
  1.FMEA.py for FMEA type files
  2.PHA.py for PHA type files
  3.SMC_QRA.py for Gap and Risk assessment files
  4.WHATIF.py for what-if type of files
  
* QRA_Main.ipynb is the main file for creating the output files.
* This file finds out which type of file it is and based up on that the matching parser will be called.
* The parser here are the 4 files listed above.
* Run the ipynb file in jupyter notebook.


